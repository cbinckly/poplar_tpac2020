try:
  import openpyxl
  print("openpyxl installed")
except ImportError as e:
  print(
      "openpyxl not installed")
  raise

wb = openpyxl.load_workbook(filename='excel_file.xlsx')
sheet = wb['Sheet1']
for i in range(1, 6):
    print(sheet['C{}'.format(i)].value)
