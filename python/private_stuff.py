class PrivateStuff():
    __priv = 12
    __password = "pass"
    def __init__(self, stuff):
        self._stuff = stuff
        self.__locked = True
    def unlock(self, _pass):
        if _pass == self.__password:
            self.__locked = False
    def lock(self):
        self.__locked = True
    def __str__(self):
        if self.__locked:
            return "Locked"
        return "{0}".format(self._stuff * self.__priv)

from datetime import datetime
class PrivateStuffs():
    __priv = 12
    __password = "pass"
    __locked = True
    unlocked_on = []
    def __init__(self, stuff):
        self._stuff = stuff
    def unlock(self, _pass):
        if _pass == self.__password:
            self.unlocked_on.append(datetime.now())
            self.__locked = False
    def lock(self):
        self.__locked = True
    def __str__(self):
        if self.__locked:
            return "Locked"
        return "{0}".format(self._stuff * self.__priv)

