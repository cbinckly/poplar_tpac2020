import time
import logging

def timeit(method):
  def timed(*args, **kwargs):
      ts = time.time()
      result = method(
                *args, **kwargs)
      te = time.time()
      print('{0}({1}, {2}) | '
            '{3:2.2f}'.format(
              method.__name__,
              args, kwargs,
              (te-ts)*1000))
      return result

  return timed


