from accpac import *

CUSTID = "1105"
REF = "Ref 0908-1-1"

def browse_to(view, **kwargs):
  query = " AND ".join(['{} = "{}"'.format(k, v)
                      for (k, v) in kwargs.items()])
  showMessageBox(query)
  return view.browse(query)

def main(*args):
  view = openView("OE0520")

  for i in range(0, 9):
    view.recordClear()
    showMessageBox("Trying view ordering {}".format(i))
    view.order(i)
    br = browse_to(view, CUSTOMER=CUSTID, REFERENCE=REF)
    if br == 0:
      showMessageBox("Browse succeded.")
      f = view.fetch()
      if f != 0:
        showMessageBox("Fetch failed")
      else:
        showMessageBox("Matched order {}".format(view.get("ORDNUMBER")))
    else:
      showMessageBox("Browse failed: {}".format(br))