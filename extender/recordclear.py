# extender/recordclear.py
from accpac import *

def main(*args):
  v = openView("AR0024")
  for i in range(0, 3):
    if v.fetch() == 0:
      showMessageBox(
        v.get("IDCUST"))

  rc = v.recordClear()
  showMessageBox(
        "RC {}".format(rc))

  if v.fetch() == 0:
    showMessageBox(
        v.get("IDCUST"))
