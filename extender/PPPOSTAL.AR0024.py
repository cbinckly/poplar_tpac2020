###############################################################################
#  A/R Customer Postal Code Enforcement - AR0024 View
#
#  This Extender view script enforces the use of a postal code of the correct
#  format A1A 1A1 when an AR Customer is in Canada.  It also validates that the
#  FSA lead number matches the province.
#
#  Parameters
#  ---------
#  None
#
#  author: Chris Binckly (2665093 Ontario Inc.)
#  email: cbinckly@gmail.com
#  Copyright 2665093 Ontario Inc. 2019
#
#  This script has been tested using the sage sample data.  If this code is
#  deployed in a live production environment it is the responsibility of the
#  End User to ensure that it is operating as required.
#
#  This software has a non-exclusive license and cannot be sublicensed, reused,
#  transferred or modified without written consent from 2665093 Ontario Inc.
###############################################################################
from accpac import *

# The stdlib re module allows the use of Regular Expressions
import re
import itertools

class Province():
    """Representation of a province's postal attributes.

    A simple struct so we can address things with meaningful names.

    :param: code - the province's two letter code (i.e. BC, QC, PE, ON...)
    :type: str
    :param: name - the province's name (i.e. British Colummbia)
    :type: str
    :param: fsas - forward sorting area prefixes (i.e. QC -> ("G", "H", "J", ))
    :param: sequence

    :returns: instance
    :rtype: Province

    :raises: None
    """

    def __init__(self, code, name, fsas):
        self.code = code
        self.name = name
        self.fsas = fsas

class CanadaPostal():
    """Representation of Canada's postal attributes.

    This class stores a map of all provice long names, codes, and FSA
    prefixes. It provides helper methods for looking up a provice based on
    user input.
    """

    PROVINCES = (
        ("AB", "Alberta", ("T", )),
        ("BC", "British Columbia", ("V", )),
        ("MB", "Manitoba", ("R", )),
        ("NB", "New Brunswick", ("E", )),
        ("NL", "Newfoundland", ("A", )),
        ("NS", "Nova Scotia", ("B", )),
        ("NT", "Northwest Territories", ("X", )),
        ("NU", "Nunavut", ("X", )),
        ("ON", "Ontario", ("K", "L", "M", "N", "P", )),
        ("PE", "Prince Edward Island", ("C", )),
        ("QC", "Quebec", ("G", "H", "J", )),
        ("SK", "Saskatchewan", ("S", )),
        ("YT", "Yukon", ("Y", )),
    )

    def __init__(self):
        self.provinces = []
        for province in self.PROVINCES:
            self.provinces.append(
                Province(province[0],
                         province[1],
                         province[2], ))

    def get_province_by_code(self, code):
        """Get a province by code (i.e. "QC").

        :param: code - two character code for the province.
        :type: str
        :returns: Province object if match found, else None
        :rtype: Province or None
        """
        for province in self.provinces:
            if province.code.lower() == code.lower():
                return province
        return None

    def get_province_by_name(self, name):
        """Get a province by name (i.e. "Quebec").

        :param: name - long name for the province.
        :type: str
        :returns: Province object if match found, else None
        :rtype: Province or None
        """
        for province in self.provinces:
            if province.name.lower() == name.lower():
                return province
        return None

    def get_province_by_fsa(self, fsa):
        """Get a province by FSA prefix (i.e. "K" for ON).

        :param: name - single character FSA prefix.
        :type: str
        :returns: Province object if match found, else None
        :rtype: Province or None
        """
        for province in self.provinces:
            if fsa.upper() in province.fsas:
                return province
        return None

    def get_province(self, data):
        """Get a provice based on some form of user input data.

        Tries matching a code, then a long name, then an FSA.

        :param: data - user input data.
        :type: str
        :returns: Province object if match found, else None
        :rtype: Province or None
        """
        by_code = self.get_province_by_code(data)
        if by_code:
            return by_code

        by_name = self.get_province_by_name(data)
        if by_name:
            return by_name

        return self.get_province_by_fsa(data)

    @property
    def codes(self):
        """Get a list of all province two character codes.

        :returns: all provice and territory two character codes.
        :rtype: list
        """

        return [p.code for p in self.provinces]

    @property
    def names(self):
        """Get a list of all province long names.

        :returns: all provice and territory long names.
        :rtype: list
        """

        return [p.name for p in self.provinces]

    @property
    def fsas(self):
        """Get a list of all valid FSA prefixes.

        :returns: flat list of all FSA prefixes.
        :rtype: list
        """

        return list(itertools.chain(*[p.fsas for p in self.provinces]))

### Utility Functions
DEBUG = True

def _debug(msg):
    if DEBUG:
        _alert("DEBUG {}\n---------\n{}".format(rotoID, msg))

def _alert(msg):
    showMessageBox("CanadaPostal\n\n{}".format(msg))

def success(*args):
    if sum(args) > 0:
        return False
    return True

CANADA_SYNONYMS = ("CA", "CAN", "CDN", "CANADA", )
FORMAT = re.compile(r'^[A-Z][0-9][A-Z] [0-9][A-Z][0-9]$')

cp = CanadaPostal()

def validate_format(postal_code):
    """Validate that the postal code has the correct syntax.

    :param: postal_code - the postal code to check.
    :returns: True if valid, else False
    :rtype: bool
    """
    if FORMAT.match(postal_code.upper().strip()):
        return True
    return False

def onBeforePut(pargs):
    """Before the value is put in the view, check that it is valid."""

    if not len(pargs.value):
        # The field is being unset, ignore.
        return Continue

    if pargs.field == "CODEPSTL":
        if me.get("CODECTRY").upper() in CANADA_SYNONYMS:

            if not validate_format(pargs.value):
                _alert("Canadian Postal Codes must be in the format A1A 1A1.")
                return Abort

            province = cp.get_province(me.get("CODESTTE"))
            if province:
                fsa = pargs.value.upper()[0]
                if fsa not in province.fsas:
                    _alert("{} postal codes begin with one of {}.".format(
                               province.name, ", ".join(province.fsas)))
                    return Abort
            else:
                _alert("{} is not a valid province or territory in Canada. "
                       "Choose from {}.".format(
                            me.get("CODESTTE"), ", ".join(cp.codes)))
                return Abort

    return Continue
