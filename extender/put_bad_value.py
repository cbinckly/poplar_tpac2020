# Put an invaluid value into a view.
from accpac import *

def main(*args, **kwargs):
  view = openView("OE0520")
  if view.fetch() == 0:
    ret = view.put("ORDNUMBER", [1, 2, 3, ])
    showMessageBox(
      "Return {}".format(ret))
  ui = UI()