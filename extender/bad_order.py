from accpac import *

ordnum = "ORD000000000002"

def main(*args, **kwargs):
  view = openView("OE0520")
  view.order(0) # ORDUNIQ
  #view.order(1) # ORDNUMBER
  view.put("ORDNUMBER", ordnum)
  
  r = view.read() # Fail if order 0!
  
  if r != 0:
    showMessageBox("Return {}".format(r))
  else:
    showMessageBox("Success!")
  UI().closeUI()
