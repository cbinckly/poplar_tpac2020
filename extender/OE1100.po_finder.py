# OE1100
# A script to replace the PO field with a finder
# for all customers with the REQUIREPO optional field set
from accpac import *

DEBUG = False

### Utility Functions

def _debug(msg):
    if DEBUG:
        showMessageBox("DEBUG {}\n---------\n{}".format(rotoID, msg))

def _alert(msg):
    showMessageBox("PO Budget Check\n\n{}".format(msg))

def success(*args):
    if sum(args) > 0:
        return False
    return True

def main(args):
    MyUI()

class MyUI(UI):
    """Custom UI to replace PO No. with finder for customers that REQUIREPO."""

    def __init__(self):
        UI.__init__(self)

        self.getControlInfo("form|afeOEORDHponumber1|stbOrderEntry",
                            self.onControlInfo)

        # Hide the default PO field
        self.po_field = self.getHostControl("afeOEORDHponumber1")
        self.po_field.hide()

        # Get the tabs to hide custom field on all tabs but orders
        self.tabs = self.getHostControl("stbOrderEntry")
        self.tabs.setOnClick(self.tabOnClick)

        # Handle and propagate changes to the custom field
        self.dsH = self.openDataSource("adsOEORDH")
        self.dsH.onAfter = self.onAfter
        self.dsH.onBeforeInsert = self.check_po_set
        self.dsH.onBeforeUpdate = self.check_po_set
        self.dsH.onBeforePost = self.check_po_set

    def onControlInfo(self, info):
        """Create the new PO No. field with finder and display the form."""

        self.po_field_info = info.find("afeOEORDHponumber1")
        self.tab_info = info.find("stbOrderEntry")

        # Create the new field, position where original PO No. field was.
        f = self.addUIField("myFecPo")
        f.controlType = "EDIT"
        f.fieldType = "STRING"
        f.caption = "PO No."
        f.labelWidth = 54
        f.left = self.tab_info.left + self.po_field_info.left
        f.top = self.tab_info.top + self.po_field_info.top
        f.width = self.po_field_info.width
        f.hasFinder = True
        f.size = 60

        f.disable()

        # Register on click/change handlers
        f.onChange = self.onChangeMyFecPo
        f.onClick = self.onClickMyFecPo
        f.show()

        self.myFecPo = f

        self.show()

    def onChangeMyFecPo(self, old_val, new_val):
        """On finder field change, propagate to underlying datasource."""
        if not old_val == new_val:
            self.dsH.dsput("PONUMBER", new_val.strip())

        return Continue

    def onClickMyFecPo(self, btnType):
        """On finder button click, open finder modal."""
        if btnType == 4:
            finder = Finder()
            finder.viewID = "PO0620"
            finder.onOK = self.customer_finder_ok
            finder.onCancel = self.customer_finder_cancel
            finder.filter = ''
            finder.displayFields = "15,16,19"
            finder.returnFields = "16"
            finder.show(self)

    def customer_finder_ok(self, e):
        """On finder OK, set the value in the underlying DS and field."""
        self.dsH.dsput("PONUMBER", e.strip())
        self.myFecPo.setValue(e.strip())
        self.setButtonClickResultToCancelWithTabAway()

    def customer_finder_cancel(self):
        """On finder cancel, return to form without changes."""
        self.setButtonClickResultToCancel()

    def tabOnClick(self, tab):
         """Show the PO No. field on the 'Orders' tab only."""
         if tab != 0:
             self.myFecPo.hide()
         else:
             self.myFecPo.show()

    def onAfter(self, eventName, fieldName, value):
        """After navigation or form changes, update field state."""
        if eventName.startswith("GO") or eventName in ["READ", "FETCH"]:
            self.myFecPo.enable()
            self.update_screen()
        if eventName == "INIT":
            self.myFecPo.disable()
            self.myFecPo.setValue('')
        if eventName == "PUT" and fieldName == "CUSTOMER":
            self.myFecPo.enable()
            self.update_screen()

    def update_screen(self):
        """Toggle the custom/default fields if whether customer requires po."""

        self.myFecPo.show()
        self.po_field.hide()

        self.myFecPo.setValue(self.dsH.get("PONUMBER"))

        return True

    def check_po_set(self):
        """Validate that the user has provided a PO number if required."""
        custid = self.dsH.get("CUSTOMER")

        if not self.dsH.get("PONUMBER"):
            _alert("Customer {} requires an active PO.  Set the PO "
                    "No. for this order and save again.".format(custid))
            return False

        return True
