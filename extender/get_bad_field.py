# Get a non-existent field
from accpac import *

def main(*args, **kwargs):
  view = openView("OE0520")
  if view.fetch() == 0:
    num = view.get("BADFIELD")
    showMessageBox(
      "Num {}".format(num))
  ui = UI()
  # ui.closeUI()
