from accpac import *

def onOpen():
    showMessageBox("View {} Program {} - me:\n\n{}".format(
        rotoID, program, dir(me)))
    return Continue