###############################################################################
#  PO Optional Field Copy - PO0620 View
#
#  This Extender view script monitors the PO0620 view, Purchase Order Headers.
#  It passes view handles for the composed optional field headers to the PO0630
#  and PO0623 view scripts.
#
#  Parameters
#  ---------
#   None
#
#  Compose Pass Throughs
#  ---------------------
#  "3000": POPORL
#  "3001": POPORLO
#  "3002": POPORHO
#
#  author: Chris Binckly (2665093 Ontario Inc.)
#  email: cbinckly@gmail.com
#  Copyright 2665093 Ontario Inc. 2019
#
#  This script has been tested using the sage sample data.  If this code is
#  deployed in a live production environment it is the responsibility of the
#  End User to ensure that it is operating as required.
#
#  This software has a non-exclusive license and cannot be sublicensed, reused,
#  transferred or modified without written consent from 2665093 Ontario Inc.
###############################################################################
from accpac import *
from extools import success
from extools.message import ExMessages
from poplar_pocpoptf import _get_view_from_handle

exm = ExMessages("PO0620", ExMessages.ERROR)

POPORHO_COMPOSE_INDEX = 4
POPORL_COMPOSE_INDEX = 1

poporho = None
poporl = None

def onBeforeCompose(event):
    global poporho
    global poporl

    if len(event.views) > POPORHO_COMPOSE_INDEX:
        poporho = event.views[POPORHO_COMPOSE_INDEX]
        poporl = event.views[POPORL_COMPOSE_INDEX]

    return Continue

def onBeforeGet(event):
    global poporho
    global poporl

    if not (poporl and poporho):
        return Continue


    if event.field == "3000":
        event.fieldType = FT_LONG
        if not poporl:
            event.value = 0
        else:
            event.value = poporl.getHandle()
        return 0

    if event.field == "3001":
        event.fieldType = FT_LONG
        poporlo = _get_view_from_handle(poporl.get("3000", FT_LONG))
        if not poporlo:
            event.value = 0
        else:
            event.value = poporlo.getHandle()
        return 0

    if event.field == "3002":
        event.fieldType = FT_LONG
        if not poporho:
            event.value = 0
        else:
            event.value = poporho.getHandle()
        return 0

    return Continue
