from accpac import *

def main(*args):
    showMessageBox("Running from scripts panel.")
    showMessageBox("User: {}".format(user))
    showMessageBox("Company: {}".format(org))
    showMessageBox("Program: {}".format(program))
    showMessageBox("rotoID (view id): {}".format(rotoID))

def onOpen():
    showMessageBox("Running from view hook.")
    showMessageBox("User: {}".format(user))
    showMessageBox("Company: {}".format(org))
    showMessageBox("Program: {}".format(program))
    showMessageBox("rotoID (view id): {}".format(rotoID))
    return Continue
