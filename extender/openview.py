# Get the first order number
from accpac import *

def main(*args, **kwargs):
  view = openView("OE0520")
  if view.fetch() == 0:
    num = view.get("ORDNUMBER")
    showMessageBox(
      "Order {}".format(num))