###############################################################################
#  Import Dialog: Import information from Excel files.
#
#  This extender UI script defines a custom dialog for importing the
#  information from an Excel file:
#  Paramters
#  ---------
#  None
#
#  Requirements
#  ------------
#  The script is intended to fulfill the following requirements:
#    - Open a dialog box that allows the user to specify the file to import.
#  Version 20190716.1
#
#  author: Chris Binckly (2665093 Ontario Inc.)
#  email: cbinckly@gmail.com
#  Copyright 2665093 Ontario Inc. 2019
#
#  This script has been tested using the sage sample data.  If this code is
#  deployed in a live production environment it is the responsibility of the
#  End User to ensure that it is operating as required.
#
#  This software has a non-exclusive license and cannot be sublicensed, reused,
#  transferred or modified without written consent from 2665093 Ontario Inc.
###############################################################################
import os
from pathlib import Path

import openpyxl

from accpac import *
from OEOrder import OEOrder

### Utility Functions

DEBUG = False
def _debug(msg):
    if DEBUG:
        showMessageBox("DEBUG {}\n---------\n{}".format(rotoID, msg))

def _alert(msg):
    showMessageBox("Import from Excel\n\n{}".format(msg))

def success(*args):
    if sum(args) > 0:
        return False
    return True

class MyUI(UI):

    # Custom control constants
    BUTTON_WIDTH = 1065
    BUTTON_SPACE = 150
    FINDER_ICON_WIDTH = 345

    def __init__(self):
        UI.__init__(self)
        self.title = "Import from Excel"

        # Register on close action.
        self.onClose = self.onCloseClick

        self.createScreen()
        self.show()

    def _check_box(self, caption, default=1):
        """Create and return a new check box object."""
        _id = caption.title().replace(" ", "")
        cb = self.addCheckBox("cb" + _id)
        cb.setText(caption)
        cb.width = 7000
        cb.setValue(default)
        return cb

    def _file_field(self, caption, default=""):
        """Create a compound field with an input field and browse button."""

        # Create labeled text input field
        _id = caption.title().replace(" ", "")
        f = self.addUIField("fileField" + _id)
        f.controlType = "EDIT"
        f.size = 250
        f.width = 5000
        f.labelWidth = 40
        f.caption = caption
        f.hasFinder = False
        if default:
            f.setValue(default)

        # Add the browse button.
        bb = self.addButton("btn{}".format(_id), "Browse...")
        bb.top = f.top
        bb.width = self.BUTTON_WIDTH
        bb.left = f.left + f.width + self.BUTTON_SPACE
        bb.onClick = self.getOnBrowseClickCallback(f)

        f.browse_btn = bb

        return (f, bb)

    def getOnBrowseClickCallback(self, field):
        """Create the browse button callback in a closure to pass the field."""
        def onBrowseClickCallback():
            dialog = OpenFileDialog()
            dialog.title = "Import Excel File"
            dialog.filter = "Microsoft Excel File (*.xls, *.xlsx)|*.xls*|All Files (*.*)|*.*"
            dialog.onOK = self.getOnFileOkCallback(field)
            dialog.show(self)

        return onBrowseClickCallback

    def getOnFileOkCallback(self, field):
        """Create the File OK callback in a closure to pass the field."""
        def onFileOkCallback(value):
            field.setValue(value)
            ui_command("return", "4")

        return onFileOkCallback

    def createScreen(self):
        """Configure and render the fields and buttons.

        # Import File: [Filepath] [Browse]

        # Buttons: Import/Close
        """

        self.import_file_field, self.import_browse_btn = self._file_field(
            "Import File", "excel_file.xslx")

        btn = self.addButton("btnImportExcel", "&Import")
        btn.top = - self.BUTTON_SPACE - btn.height
        btn.width = self.BUTTON_WIDTH
        btn.left = 2 * (-self.BUTTON_WIDTH - self.BUTTON_SPACE)
        btn.onClick = self.onImportClick
        self.btnImport = btn

        btn = self.addButton("btnClose", "&Close")
        btn.top = self.btnImport.top
        btn.width = self.BUTTON_WIDTH
        btn.left = -btn.width - self.BUTTON_SPACE
        btn.onClick = self.onCloseClick
        self.btnClose = btn

    def onCloseClick(self):
        """Close the UI if the Close button or window X are clicked."""
        self.closeUI()

    def getImportFilePathObject(self):
        filepath_components = self.import_file_field.getValue().split("\\")
        return Path(filepath_components[0], os.sep, *filepath_components[1:])

    def openAndValidateFirstWorksheet(self):
        """Open the first worksheet in the workbook at import_file_field and
        validate basic sheet properties.

        :returns: openpyxl.Worksheet
        """

        filepath = self.getImportFilePathObject()

        try:
            wb = openpyxl.load_workbook(str(filepath))
        except Exception as err:
            _alert("Couldn't open workbook at {}.".format(filepath))
            return

        if len(wb.worksheets) > 0:
            ws = wb.worksheets[0]
        else:
            _alert("Unable to find any worksheets in {}.".format(filepath))
            return

        if ws.max_row < 2:
            _alert("The first worksheet has only a header, no data, "
                   "rows in {}".format(filepath))
            return

        if ws.max_column < 3:
            _alert("The first worksheet has only {} columns.  A minimum of 3 "
                   "are required.".format(ws.max_column))
            return

        return ws

    def onImportClick(self):
        """Import the entries from the file."""

        total = 0
        updated = 0
        field_fail_msg = "Failed to set field '{}' to '{}' for {}, skipping."

        # Get the worksheet to import from.
        ws = self.openAndValidateFirstWorksheet()
        if not ws:
            # If the worksheet isn't valid an error has already been displayed.
            return

        # Iterate over the rows, skipping the header, and update the OEOrder.
        seen_header = False
        for row in ws.rows:
            showMessageBox(row[2].value)

def main(*args, **kwargs):
    MyUI()
