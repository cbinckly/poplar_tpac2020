# AP2100
# Add a misc line to an invoice after a trigger line
# is entered.
from accpac import *

DEBUG = False

TRIGGER_GLACCT = "1300"

ADD_GLACCT = "6380"
ADD_DIST = "MISC"

MISC_PERCENT = 12

def _debug(msg):
    if DEBUG:
        _alert("DEBUG\n---------\n{}".format(msg))

def _alert(msg):
    showMessageBox("Tax Rebate\n\n{}".format(msg))

def success(*args):
    if sum(args) > 0:
        return False
    return True

def main(args):
    MiscChargeUI()

class MiscChargeUI(UI):
    """Custom UI to add new lines to AP Invoice entry."""

    def __init__(self):
        UI.__init__(self)
        self.adsAPIBD = self.openDataSource('adsAPIBD')
        self.adsAPIBD.onAfterInsert = self.apibdOnAfterInsert
        self.show()

    def apibdOnAfterInsert(self):
        _debug("apidbOnAfterInsert: {}".format(self.insert_count))

        # Get the item G/L Account
        glacct = self.adsAPIBD.get("IDGLACCT")
        if glacct.strip() != TRIGGER_GLACCT:
            return Continue

        amtdist = self.adsAPIBD.get("AMTDIST")

        new_line_cost = amtdist * MISC_PERCENT
        _debug("Creating {} with amount {}".format(
            ADD_GLACCT, new_line_cost))

        self.adsAPIBD.dsrecordgenerate()
        self.adsAPIBD.dsput("IDDIST", "MISC")
        self.adsAPIBD.dsput("IDGLACCT", account)
        self.adsAPIBD.dsput("AMTDIST", new_line_cost)
        self.adsAPIBD.dsinsert()
