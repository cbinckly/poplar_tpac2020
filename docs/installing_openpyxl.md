# Installing Packages with pip

This document describes how to install and configure the ORDERXLS Extender 
module, including installing the Python dependencies. 

## Install Python

In order to install the dependencies, download Python 3.4.2 and install it on 
the workstation.

1. Download the installer for Python 3.4.2. on Windows:
https://www.python.org/downloads/release/python-342/

2. Run the installer, installing into `C:\Python34`
    - Note: if another path is chosen, substitute it in all commands that 
      follow.

3. Only the optional `pip` component must be installed, all other components
may be skipped.
    - A full installation requires ~ 73MB, a minimal installation ~ 32MB

## Check pip Version

Python 3.4.2 is distributed with a version of pip that uses unsafe encryption
to communicate with the package index.  As a consequence, it has 
been deprecated and may not run.

In the shell of your choice, try running the following:

```
PS C:\Users\Administrator> C:\Python34\python.exe -m pip list
```

If this command returns nothing at all or raises an error, it is likely
that the pip version is out of date.

To upgrade pip to a supported version, use the `get-pip.py` script for 
Python 3.4.  Note that the version of `get-pip.py` currently being 
distributed online no longer supports 3.4, so I have included a 
supported version in the accompanying `python/` directory.

```
PS C:\Users\Administrator> C:\Python34\python.exe get-pip.py
```

## Install the Packages using pip

Once Python3.4 is installed, use the pip Python package manager to install 
openpyxl and its dependencies.

1. Start a Command or PowerShell prompt.

2. Navigate to the `VI67A\python` directory.
    - `cd C:\Sage\Sage300\vi67a\python`

3. Use the pip module to update pip and install openpyxl into the Extender 
embedded python deployment.

```
PS C:\Users\Administrator> cd C:\Sage\Sage300\vi67a\python\

PS C:\Sage\Sage300\vi67a\python> C:\Python34\python.exe -m pip install -U pip -t .\lib\

You are using pip version 6.0.8, however version 19.1.1 is available.  
You should consider upgrading via the 'pip install --upgrade pip' command.  
Collecting pip from https://files.pythonhosted.org/packages/5c/e0/be401c003291b56efc55aeba6a80ab790d3d4cece2778288d65323009420/pip-19.1.1-py2.py3-none-any.whl#sha256=993134f0475471b91452ca029d4390dc8f298ac63a712814f101cd1b6db46676  
Downloading https://files.pythonhosted.org/packages/5c/e0/be401c003291b56efc55aeba6a80ab790d3d4cece2778288d65323009420/pip-19.1.1-py2.py3-none-any.whl (1.4MB)  
Installing collected packages: pip  
Found existing installation: pip 6.0.8  
    Uninstalling pip-6.0.8:  
        Successfully uninstalled pip-6.0.8  
Successfully installed pip-19.1.1  
```

```
PS C:\Users\Administrator> cd C:\Sage\Sage300\vi67a\python\

PS C:\Sage\Sage300\vi67a\python> C:\Python34\python.exe -m pip install -U openpyxl -t .\lib\

Collecting openpyxl  
  Downloading https://files.pythonhosted.org/packages/cf/63/811f01cd0e473e2d2ee2b257bf2ae95efaf960c4bee74195e331e8e139e2/openpyxl-2.5.14.tar.gz (173kB)  
Collecting jdcal (from openpyxl)  
  Downloading https://files.pythonhosted.org/packages/f0/da/572cbc0bc582390480bbd7c4e93d14dc46079778ed915b505dc494b37c57/jdcal-1.4.1-py2.py3-none-any.whl  
Collecting et_xmlfile (from openpyxl)  
  Downloading https://files.pythonhosted.org/packages/22/28/a99c42aea746e18382ad9fb36f64c1c1f04216f41797f2f0fa567da11388/et_xmlfile-1.0.1.tar.gz  
Installing collected packages: jdcal, et-xmlfile, openpyxl  
  Running setup.py install for et-xmlfile: started  
    Running setup.py install for et-xmlfile: finished with status 'done'  
  Running setup.py install for openpyxl: started  
    Running setup.py install for openpyxl: finished with status 'done'  
Successfully installed et-xmlfile-1.0.1 jdcal-1.4.1 openpyxl-2.5.14  
```

The Python dependencies are now installed in the extender environment.

### Optional: Remove Python

Although not required, the Python installation should be removed in production
environments once the openpyxl package has been installed. To remove Python3.4:

1. Launch the Python3.4 uninstaller from the start menu and follow the prompts
to remove the application.
2. Manually delete the `C:\Python34` directory.
3. Manually delete the Python installer file.

## Install and Configure an Extender Module

From the Sage 300 Desktop:

1. Navigate to the Extender section and open the Module screen.
2. Click Import Module.
3. Locate and import the .vi module file.
4. Close the Modules screen.
5. If the module required configuration in the database, 
   Open the Custom Table Editor.
    - Load the module table
    - Add any relevant configuration

## Add an Icon to the Desktop

An icon can be added anywhere on the Sage desktop to launch scripts installed
by the module.

To add an icon.

1. Open the Extender Scripts screen.
2. Highlight the script you'd like to add, right-click, and select 
    "Add Icon to Desktop".
3. Provide a custom name for the icon and select where in the desktop to 
    display it.
4. Save the changes. Verify that the icon works as expected.


## Removing an Icon from the Desktop

To remove an icon.

1. Open the Extender Scripts screen.
2. Highlight any script, right-click, and select 
    "Add Icon to Desktop".
3. In the Icon Tree pane, highlight the icon to be removed.
4. Push the delete (Del) key on your keyboard to remove the icon.
5. Close the window using the window 'x' button, *not the 'Close' button*
