# Install Git SCM

Although not required, you can use [Git](https://git-scm.com) to get access to
additional code examples and to enable development process automation
workflows.

It also provides access to a BASH-like shell that can make working with Python
scripts a little easier than the Windows Command Line or PowerShell.

## Get the Installer

Start by downloading the most recent installer from https://git-scm.com/downloads

![Git Download Page](https://s3.amazonaws.com/dev.poplars.tpac2020/install_git_download.png)

## Install Git

Run the installer. Git is a fully features source control management tool made
to enable developers to collaborate on projects at any scale.  Think things 
like the Linux kernel. It has a rich configuration and lots of options!

If you aren't already a git user, I recommend accepting most of the installer
defaults.  There are a few choices that you should make for yourself and won't
impact your ability to use Git during the course.

First of all, whether to include the Windows Explorer integration.  You can
safely skip the Windows Explorer integration if you don't want to clutter
your context menus.

![Customize Git Installation]("https://s3.amazonaws.com/dev.poplars.tpac2020/install_git_customize.png")

After that, select an editor you know and are comfortable with. After years of
learning, I have come to prefer `vim` but for most users `Notepad++` or `Atom`
are likely better choices.

![Git Installation Choose Editor]("https://s3.amazonaws.com/dev.poplars.tpac2020/install_git_editor.png")

As with Python, Git does not need to be installed in your system path.  
However, doing so will make your life much easier.  It is your choice, 
beware the third option unless you know what replacing the system utilities
in the system path entails. I recommend using Git from the command line and
third party software.

![Git Installation System Path]("https://s3.amazonaws.com/dev.poplars.tpac2020/install_git_path.png")

For all other options, select the defaults unless you know what you're doing.

## Verify the Installation

To confirm that the installation completed successfully, open the Git Bash
prompt and get the installed git version.

![Start Menu Entry for Git Bash](https://s3.amazonaws.com/dev.poplars.tpac2020/install_git_start_bash.png)

Once the console starts, verify the git version.

![Git Version!](https://s3.amazonaws.com/dev.poplars.tpac2020/install_git_check_version.png)

That is it, your Git installation is complete!  Thanks for 
taking the time to come prepared.
