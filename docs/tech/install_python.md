# Install Python 3.4.2

During the course we will be using Python interactively.  To 
participate you'll need Python 3.4.2 installed on your laptop.

Installing Python 3.4.2 is easy, won't interfere with your system or 
other Python installations (such as that used by Extender), and is
easy to undo.

## Get the Installer

Start by downloading the Python 3.4.2 installer from python.org:
https://www.python.org/downloads/release/python-342/

![Python 3.4.2 Download Page](https://s3.amazonaws.com/dev.poplars.tpac2020/install_python_download_page.png)

Select the version that matches your platform. I am using a x64 VM but 32-bit
installers and other platforms are also available.

![Python 3.4.2 Installer Files](https://s3.amazonaws.com/dev.poplars.tpac2020/install_python_download_files.png)

## Install Python

Run the installer and accept the defaults.  You may install for a specific user
or for everyone.

![Python Installer Prompt](https://s3.amazonaws.com/dev.poplars.tpac2020/install_python_for_whom.png)

When prompted to customize the installation, do not install Python into the 
system path.  This should be the default but verify before proceeding.

![Python Installation Customization](https://s3.amazonaws.com/dev.poplars.tpac2020/install_python_dont_add_to_path.png)

Run the installer through to completion.

## Verify the Installation

To confirm that the installation completed successfully, open the Python 3.4.2
command line from the new start menu entry.

![Start Menu Entry for Python Command Line](https://s3.amazonaws.com/dev.poplars.tpac2020/install_python_start_cmdline.png)

Once the console starts, do the `Hello World!` thing...

![Hello World in Python!](https://s3.amazonaws.com/dev.poplars.tpac2020/install_python_hello_world.png)

That is it, your Python installation is ready for the training!  Thanks for 
taking the time to come prepared.
